import React from 'react';

import { Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';

import { LoginScreen, TripsScreen, ListTripScreen } from './src/screens';

import { UrlLogin } from "./src/lib/constants";

const MainNavigation = createDrawerNavigator();

const MyTheme = {
	dark: false,
	colors: {
	  primary: '#5d8a50',
	  background: 'rgb(242, 242, 242)',
	  card: '#333',
	  text: '#dedede',
	  border: '#fff',
	  notification: '#0f0',
	},
  };

class App extends React.Component {

	state = {
		pantalla: 'login',
		user: "",
		message: ""
	}

	componentDidMount() {
		this.getLoginData();
	}

	/**
	 * ejecuta el proceso de inicio de sesion desde el WS 
	 * @param {string} user el nombre de usuario para verificar su inicio de sesion
	 * @param {string} password la contrasena del usuario
	 */
	handleLogin = async (user, password) => {
		if(typeof(user) === "string" && typeof(password) === "string" ) {
			if ( user.length > 0 ) {
				let datos = new Object();
				datos.username = user;
				datos.password = password;
				try {
					let response = await fetch(UrlLogin, {
						method: 'POST', // or 'PUT'
						body: JSON.stringify(datos), // data can be `string` or {object}!
						headers:{
							'Content-Type': 'application/json'
						}
					}).then(res => res.json()).catch(error =>{ return '{"status":"error"}' })
					.then(response => { return response; });

					if ( typeof(response) === 'object' ) {
						if ( response.status === 'ok' ) {
							if ( response.data.role === '3' ) {
								this.storeLoginData( response.data.id, user, password );
								this.setState({ pantalla: "app", user: response.data.id });
							}
							else {
								Alert.alert(
									"Mexicana Car Service", "You are not allowed to use this app.",[ { text: "OK",  } ], { cancelable: false }
								);
							}
						}
						else {
							Alert.alert(
								"Mexicana Car Service", response.message,[ { text: "OK",  } ], { cancelable: false }
							);
						}
					}
				}
				catch {
					Alert.alert("Mexicana Car service", "Network error. Please connect to internet.");
				}
			}
		}
	}

	/**
	 * llama a la siguiente ventana despues de hacer un registor correcto de usuario
	 */
	handleRegistration = () => {
		this.setState({pantalla: 'login'});
	}

	/**
	 * llama a la pantalla para realizar un registro de usuario
	 */
	signup = () => {
		this.setState({pantalla: 'register'});
	}

	/**
	 * 
	 * @param {string} idUser el ID del usuario loggeado
	 * @param {string} user el username del usuario loggeado
	 * @param {string} password el password del usuario loggeado
	 */
	storeLoginData = async (idUser, user, password) => {
		try {
			let value = {
				id: idUser, user: user, password: password
			}
			let jsonValue = JSON.stringify(value)
			await AsyncStorage.setItem('@logindriver', jsonValue)
		} catch (e) {
			console.log('error setting login data');
		}
	}

	/**
	 * busca en el almacenamiento del telefono la informacion del usuario loggeado
	 * y si no encuentra manda la pantalla de inicio de sesion
	 */
	getLoginData = async () => {
		try {
			let value = await AsyncStorage.getItem('@logindriver');
			console.log( "se ha encontrado el valor en el login" );
			console.log(value); console.log("\n");
			if(value !== null) {
				let loginInfo = JSON.parse( value );
				if( typeof(loginInfo) == 'object' ) {
					this.handleLogin ( loginInfo.user, loginInfo.password );
				}
				else {
					this.setState({pantalla: 'login'});
				}
				
			}
		} catch(e) {
			console.log('error getting login info');
			this.setState({pantalla: 'login'});
		}
	}

	/**
	 * cierra la sesion en el actual dispositivo
	 */
	handleLogout = async () => {
		try {
			console.log( "LOGOUT" );
			await AsyncStorage.removeItem('@logindriver');
			this.setState({pantalla: 'login'});
		} catch(e) {
			this.setState({pantalla: 'login'});
		}
	}

	render() {

		if ( this.state.pantalla == 'app' ) {
			return (
				<NavigationContainer theme={MyTheme}>
					<MainNavigation.Navigator initialRouteName="Home">
						<MainNavigation.Screen name="Trips" options={{title: "Trips"}}>
							{props => <TripsScreen {...props} /> }
						</MainNavigation.Screen>
						<MainNavigation.Screen name="History" options={{title: "List"}}>
							{props => <ListTripScreen {...props} /> }
						</MainNavigation.Screen>
						
					</MainNavigation.Navigator>
				</NavigationContainer>
			);
		}
		else if ( this.state.pantalla == 'register' ) {
			return (
				<RegisterScreen handleRegistration={this.handleRegistration} handleBack={this.handleLogout} />
			);
		}	
		else {
			return (
				<LoginScreen handleLogin={this.handleLogin} signup={this.signup} />
			);
		}
		
	}

}

export default App;
