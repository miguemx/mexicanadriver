import React from "react";
import { View, TouchableHighlight, Text } from "react-native";

import { TripsStyles } from "../styles";

export default class TripItem extends React.Component {
    render() {
        console.log( this.props );
        return(
            <TouchableHighlight style={TripsStyles.itemContainer} onPress={this.props.selectTrip}>
                <View style={TripsStyles.itemListElementContainer}>
                    <Text style={TripsStyles.textStreetList}>{this.props.street}</Text>
                    <Text style={TripsStyles.textMinutesList}>{this.props.myDistance} km</Text>
                </View>
            </TouchableHighlight>
            
        );
    }
}