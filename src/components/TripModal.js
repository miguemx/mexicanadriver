import React from "react";

import {
    View,
    ScrollView,
    Text,
    TouchableOpacity,
    Image,
    Modal,
} from "react-native";
import { TripsStyles } from "../styles";

import { UrlViewTrip } from "../lib/constants"

export default class TripModal extends React.Component {

    state = {
        trip: {},
    }

    componentDidMount() {
    }

    componentDidUpdate() {
        if ( this.props.showTrip ) {
            if( typeof(this.state.trip.trip) == 'undefined' ) {
                this.getTrip();
            }
        }        
    }

    /**
     * obtiene la informacion del viaje para poner los datos y empezarlo
     */
    getTrip = async () => {
        console.log("Rendeerizar este trip");
        console.log( this.props.trip.id );
        let url = UrlViewTrip + this.props.trip.id;
        let response = await fetch(url, {
                method: 'GET', // or 'PUT'
                headers:{
                    'Content-Type': 'application/json'
                }
        }).then(res => res.json()).catch(error => { return '{"status":"error"}' }).then(response => { return response; });
        
        if ( typeof(response) === 'object' ) {
            if ( response.status === 'ok' ) {
                this.setState({ trip: response.data });
            }
        }
    }

    endTrip = async () => {
        let res = await this.props.endTrip(this.state.trip.destination.mapsCoord);
        if ( res ) {
            this.setState({ trip: {} });
        }
    }

    render() {
        let tripText = {
            pickupStreet: 'Loading...',
            pickupNumber: '',
            destinationStreet: 'Loading...',
            destinationNumber: '',
            eta: 'Loading...',
            distance: 'Loading...',
            amount: 'Loading...'
        };
        if( typeof(this.state.trip.trip) !== 'undefined' ) {
            tripText = {
                pickupStreet: this.state.trip.pickup.street,
                pickupNumber: this.state.trip.pickup.number,
                destinationStreet: this.state.trip.destination.street,
                destinationNumber: this.state.trip.destination.number,
                eta: this.state.trip.trip.eta,
                distance: this.state.trip.trip.distance,
                amount: this.state.trip.trip.amount
            };
        }
        return(
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.props.showTrip}
                supportedOrientations={['portrait', 'landscape']}  >
                
                <View style={GlobalStyles.modalContainer}>
                    <ScrollView style={TripsStyles.tripDataContainer}>
                        <View style={TripsStyles.pointDataContainer}>
                            <Text style={TripsStyles.infoTripLabel}>Pick up</Text>
                            <Text style={TripsStyles.infoTripData}>
                                {tripText.pickupNumber} {tripText.pickupStreet}
                            </Text>
                            <TouchableOpacity onPress={this.props.notifyClient} >
                                <Text>Notify Client</Text>
                            </TouchableOpacity>
                        
                        </View>

                        <View style={TripsStyles.pointDataContainer}>
                            <Text style={TripsStyles.infoTripLabel}>Destination</Text>
                            <Text style={TripsStyles.infoTripData}>
                                {tripText.destinationNumber} {tripText.destinationStreet}
                            </Text>
                        
                        </View>

                        <View style={TripsStyles.pointDataContainer}>
                            <Text style={TripsStyles.titleSection}>Trip Overview</Text>

                            <Text style={TripsStyles.infoTripLabel}>ETA</Text>
                            <Text style={TripsStyles.infoTripData}>
                                {tripText.eta}
                            </Text>

                            <Text style={TripsStyles.infoTripLabel}>Distance</Text>
                            <Text style={TripsStyles.infoTripData}>
                                {tripText.distance}
                            </Text>

                            <Text style={TripsStyles.infoTripLabel}>Amount</Text>
                            <Text style={TripsStyles.infoTripData}>
                                {tripText.amount}
                            </Text>
                        
                        </View>
                            <TouchableOpacity style={TripsStyles.pointDataContainer}>
                                <Text>Cancel trip</Text>
                            </TouchableOpacity>
                        
                    </ScrollView>
                    
                    <View style={TripsStyles.tripButtonContainer}> 
                        {
                            (this.props.ongoing===false)?
                            <TouchableOpacity onPress={this.props.toClientLocation} style={GlobalStyles.iconButton}>
                                <Image source={require('../../assets/tooltip-account.png')} style={GlobalStyles.iconButtonImage} />
                                <Text style={TripsStyles.tripButtonText}>Pickup Client</Text>
                            </TouchableOpacity>
                            : null
                        }
                        
                        {
                            (this.props.ongoing===false)?
                            <TouchableOpacity onPress={this.props.startTrip} style={GlobalStyles.iconButton}>
                                <Image source={require('../../assets/navigation.png')} style={GlobalStyles.iconButtonImage} />
                                <Text style={TripsStyles.tripButtonText}>Start Trip</Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity onPress={this.endTrip} style={GlobalStyles.iconButton}>
                                <Image source={require('../../assets/map-marker-check.png')} style={GlobalStyles.iconButtonImage} />
                                <Text style={TripsStyles.tripButtonText}>End Trip</Text>
                            </TouchableOpacity>
                        }
                        
                    </View>
                    
                </View>
            </Modal>
        );
        
    }

}