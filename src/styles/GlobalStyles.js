import { StyleSheet } from "react-native";

export default GlobalStyles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "flex-start",
        justifyContent: "flex-start",
        backgroundColor: "#232e25"
    },
    inner : {
        flex: 1,
        width: "100%",
    },

    header : {
        alignItems: 'center',
        marginBottom: 5,
        marginTop: 35,
        borderBottomColor: "#dedede",
        borderBottomWidth: 2,
        paddingBottom: 5,
    },
    textHeader : {
        fontSize: 22,
        color:  "#fff"
    },

    body: {
        flex: 1,
    },

    footer: {
        borderTopWidth: 1,
        borderTopColor: "#fff",
        marginBottom: 10,
        paddingTop: 5,
        alignItems: "center",

    },

    footerHorizontal: {
        borderTopWidth: 1,
        borderTopColor: "#fff",
        marginBottom: 10,
        flexDirection: "row",
    },

    modalContainer: {
        flex: 1,
        alignItems: "flex-start",
        justifyContent: "center",
        marginTop: 60,
        backgroundColor: "#232e25",
        borderTopColor: "#ababab",
        borderTopWidth: 2,
        paddingBottom: 0,
    },

    iconButton : {
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        margin: 10
    },
    iconButtonImage: {
        width: 40,
        height: 40,
    },

    textNormal: {
        color: "#c7ffd1",
        fontSize: 17
    },
    
    link : {
        color: "#c7ffd1",
        fontSize: 17,
        textDecorationLine: "underline"
    }

});