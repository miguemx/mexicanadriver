import { StyleSheet } from "react-native";

export default TripsStyles = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%",
        
    },
    itemContainer: {
        padding: 10,
        backgroundColor: "#b5e6ba",
        borderColor: "#4d8a53",
        borderRadius: 10,
        margin: 10
    },
    itemListElementContainer: {
        flexDirection: "row"
    },
    textStreetList : {
        fontSize: 24,
        fontWeight: "bold",
        flex: 1
    },
    textMinutesList: {
        fontSize: 16,
        color: "#333",
    },

    tripDataContainer: {
        width: "100%",
        flex: 1,        
        padding: 10,
    },

    titleSection: {
        fontSize: 24,
        fontWeight: "bold",
        marginBottom: 10,
        borderBottomColor: "#dedede",
        borderStyle: "dotted"
    },

    pointDataContainer: {
        width: "100%",
        padding: 10,
        borderRadius: 10,
        backgroundColor: "#a1d1a6",
        marginBottom: 10,
        borderWidth: 1,
        borderColor: "#5b825f"
    },

    infoTripLabel: {
        
        fontWeight: "bold",
        fontSize: 18,
    },
    infoTripData: {
        marginBottom: 10,
        fontSize: 18,
    },
    tripButtonContainer: {
        flexDirection: "row",
        width: "100%",
        padding: 10,
        justifyContent: "space-around",
        backgroundColor: "#232e25",
        borderTopWidth: 1,
        borderTopColor: "#dedede"
    },
    tripButtonText : {
        color: "#b5e6ba",
    },

    tripListContainer : {
        flex: 1,
        flexDirection: "column",
    },
    tripListList: {
        backgroundColor: "#0f0",
    },
    tripList : {
        flex: 1
    },
    tripListStatus : {
        borderTopWidth: 1,
        borderTopColor: "#dedede",
        paddingTop: 5,
        paddingBottom: 20,
        paddingLeft: 20, 
        paddingRight: 20,
        width: "100%",
        justifyContent: "flex-end"
    }

});