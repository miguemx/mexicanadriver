import { StyleSheet } from "react-native";

export default FormsStyles = StyleSheet.create({

    input : {
        width: "100%",
        borderWidth: 1,
        borderColor: "#787878",
        padding: 5,
        color: "#003009",
        marginBottom: 10,
        backgroundColor: "#fff",
        borderRadius: 5,
    },

    label:{
        color: "#c7ffd1",
        fontSize: 20,
        marginBottom: 5,
        width: "100%",
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        
    },

    bigButton : {
        width: "100%",
        padding: 5,
        marginBottom: 5,
        backgroundColor: "#036113",
        justifyContent: "center",
        flexDirection: "row"
    },

    bigButtonDisabled : {
        width: "100%",
        padding: 5,
        marginBottom: 5,
        backgroundColor: "#353535",
        justifyContent: "center",
        flexDirection: "row"
    },

    bigButtonText: {
        color: "#fff",
        fontSize: 20,
        fontWeight: "bold"
    }

});

