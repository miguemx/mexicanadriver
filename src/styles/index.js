export { default as GlobalStyles } from "./GlobalStyles";
export { default as TripsStyles } from "./TripsStyles";
export { default as FormsStyles } from "./FormsStyles";
export { default as LoginStyles } from "./LoginStyles";