// -----------------------------------------------------------------------|
// URLS para los web services                                             |
// -----------------------------------------------------------------------|

const UrlDomain = "http://api.privatecarservise.com/";
// const UrlDomain = "http://localhost/mexicana-api/public/";

export const APPNAME = "Mexicana Car Service";

export const UrlLogin = UrlDomain + "Login/Login";
export const UrlRegisterUser = UrlDomain + "Users/Crea";

export const UrlRequestTrip = UrlDomain + "Calls/Crea";
export const UrlTakeTrip = UrlDomain + "Trips/Take";
export const UrlStartTrip = UrlDomain + "Trips/Inicia";
export const UrlEndTrip = UrlDomain + "Trips/Finaliza";
export const UrlViewTrip = UrlDomain + "Trips/Ver/";

export const UrlUserProfile = UrlDomain + "Users/Ver/";

export const UrlCards = UrlDomain + "Customers/Cards/";
export const UrlCardAdd = UrlDomain + "PaymentMethods/AddCard";
export const UrlCardDefault = UrlDomain + "PaymentMethods/MakeCardDefault";
export const UrlCardRemove = UrlDomain + "PaymentMethods/DeleteCard";

export const UrlSocket = "ws://migue.com.mx";


// -----------------------------------------------------------------------|
// Textos de meses y dias, y calculo de años                              |
// -----------------------------------------------------------------------|
export const Months = [
    { key: "1", value: "1", label: "January" },
    { key: "2", value: "2", label: "February" },
    { key: "3", value: "3", label: "March" },
    { key: "4", value: "4", label: "April" },
    { key: "5", value: "5", label: "May" },
    { key: "6", value: "6", label: "June" },
    { key: "7", value: "7", label: "July" },
    { key: "8", value: "8", label: "August" },
    { key: "9", value: "9", label: "September" },
    { key: "10", value: "10", label: "October" },
    { key: "11", value: "11", label: "November" },
    { key: "12", value: "12", label: "December" },
];

export const Years = calculaAnios(10);








// -----------------------------------------------------------------------|
// funciones de ayuda                                                     |
// -----------------------------------------------------------------------|
/**
 * obtiene el numero de anios a partir del actual y regresa un objeto que pueda ser renderizable
 * por componentes react
 * @param {int} numYears el numero de anios a obtener 
 * @return {array} un arreglo de objetos con los anios renderizables por react
 */
function calculaAnios (numYears) {
    let currDate = new Date();
    let currYear = currDate.getFullYear();
    let years = new Array();
    for ( let i=0; i<=numYears; i++ ) {
        let yearValue = currYear + i;
        years.push({
            key: yearValue,
            label: yearValue,
            value: yearValue
        });
    }
    return years;
}


