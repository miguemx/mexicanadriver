import React from "react";
import { 
    View, 
    TextInput, 
    Text, 
    ActivityIndicator, 
    KeyboardAvoidingView, 
    TouchableWithoutFeedback, 
    Keyboard 
} from "react-native";

import { GlobalStyles, LoginStyles, FormsStyles } from "../styles/"
import { TouchableOpacity } from "react-native-gesture-handler";

export default class LoginScreen extends React.Component {

    state = {
        user: "",
        password: "",
        message: "",
        loging: false,
    }

    componentDidMount() {

    }

    /**
     * invoca al inicio de sesion e indica al usuario lo que ocurre.
     * si hay inicio de sesion exitoso, guarda la llave local para
     * no volver a pedirlo
     */
    login = async () => {
        this.setState({message: "", loging: true});
        if ( this.state.user.length > 0 && this.state.password.length > 0 ) {
            await this.props.handleLogin( this.state.user, this.state.password );
            this.setState({loging: false});
        }
        else {
            this.setState({message: "Please provide your username and password.", loging: false});
        }
    }

    voidnull = () => {}

    render() {
        return(
            <KeyboardAvoidingView behavior={Platform.OS == "ios" ? "padding" : "height"} style={GlobalStyles.container}>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <View style={GlobalStyles.inner}>
                        <View style={GlobalStyles.header}>
                            <Text style={GlobalStyles.textHeader}>Driver's app</Text>
                        </View>
                        <View style={GlobalStyles.body}>
                            <Text style={FormsStyles.label}>User</Text>
                            <TextInput 
                                placeholder="ID" 
                                onChangeText={ (user) => this.setState({user}) } 
                                autoCapitalize='none'
                                style={FormsStyles.input} />
                            
                            <Text style={FormsStyles.label}>Password</Text>
                            <TextInput placeholder="Password" 
                                onChangeText={ (password) => this.setState({password}) } 
                                secureTextEntry={true}
                                style={FormsStyles.input} />
                            
                            {
                                (this.state.loging == false)?
                                <TouchableOpacity onPress={ () => this.login() } style={FormsStyles.bigButton}>
                                    <Text style={FormsStyles.bigButtonText}>Login</Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity onPress={ this.voidnull } style={FormsStyles.bigButtonDisabled}>
                                    <Text style={FormsStyles.bigButtonText}>Loging in</Text>
                                    <ActivityIndicator />
                                </TouchableOpacity>
                            }

                            <Text style={LoginStyles.notify}>{this.state.message}</Text>
                        </View>
                        <View style={GlobalStyles.footer}>
                            <Text style={GlobalStyles.textNormal}>Do you want to become a driver?</Text>
                            <TouchableOpacity onPress={this.createAccount}><Text style={GlobalStyles.link}>Click here</Text></TouchableOpacity>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>



            
        );
    }
}