import React from "react";
import AsyncStorage from '@react-native-community/async-storage';
import { 
    View, 
    FlatList, 
    Text, 
    
} from "react-native";

export default class ListTripScreen extends React.Component {

    state = {
        driverId: null,
        trips: []
    }


    render() {
        let statusLocation = "Off"; let statusSocket = "Off"; let statusApp = "Off";
        if ( this.state.location != null ) { statusLocation = "On"; }
        if (this.state.socketconnection != false) { statusSocket = "On"; }
        if (this.state.driverId != false) { statusApp = "On"; }
        return(
            <View style={GlobalStyles.container}>
                <View style={TripsStyles.container}>
                    <View style={GlobalStyles.header}>
                        <Text style={GlobalStyles.textHeader}>Trip list</Text>
                    </View>
                    
                    <FlatList
                        data={this.state.trips}
                        renderItem={ ({item}) => <TripItem street={item.pickupstreet} eta={item.distance} myDistance={item.myDistance} selectTrip={ () => this.selectTrip(item) } /> }
                        keyExtractor={ item => item.id.toString() } />
                    
                    <View style={TripsStyles.tripListStatus}>
                        <Text style={GlobalStyles.textNormal}>Scroll up and down to see your trips. Tap on trip for details.</Text>
                    </View>

                </View>
                
            </View>
        );
    }
}