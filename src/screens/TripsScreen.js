import React from "react";
import AsyncStorage from '@react-native-community/async-storage';
import { 
    View, 
    FlatList, 
    Linking, 
    Text, 
    Modal, 
    Image, 
    Alert,
    TouchableOpacity,
    ScrollView
} from "react-native";

import * as Permissions from "expo-permissions";
import * as Location from "expo-location";
import { Notifications } from "expo";

import getKilometros from "../lib/distance"

import { UrlSocket, APPNAME, UrlTakeTrip, UrlStartTrip, UrlEndTrip } from "../lib/constants";

const socket = new WebSocket( UrlSocket );

// const io = require('socket.io-client');
// const socket = io(UrlSocket, { transports: ['websocket'], });

import TripItem from "../components/TripItem";
import TripModal from "../components/TripModal"
import { GlobalStyles, TripsStyles } from "../styles";

const getToken = async () => {
    const { status } = await Permissions.getAsync( Permissions.NOTIFICATIONS );
    if ( status !== 'granted' ) {
        return;
    }
    const token = Notifications.getExpoPushTokenAsync();
    return token;
}

export default class TripsScreen extends React.Component {

    state = {
        driverId: null,
        trips: [],
        currentTrip: {},
        location: null,
        onTrip: false,
        ongoing: false,
        notified: false,
        socketconnection: false,
    }

    /**
     * fires when screen finish loading
     */
    componentDidMount() {
        this.getLoginData();
        getToken();
        this.setSocketConnection();
        setInterval( this.getLocation, 10000 );
        this.getLocation();
    }

    /**
	 * busca en el almacenamiento del telefono la informacion del usuario loggeado
	 * y si no encuentra manda la pantalla de inicio de sesion
	 */
	getLoginData = async () => {
		try {
			let value = await AsyncStorage.getItem('@logindriver');
			if(value !== null) {
				let loginInfo = JSON.parse( value );
				if( typeof(loginInfo) == 'object' ) {
					this.setState({ driverId: loginInfo.id });
				}
			}
		} catch(e) {
		}
	}


    /**
     * sets socket connection to central server
     * on connect sets connection
     * on newcall receives new trip data and fires renderNewTrip
     */
    setSocketConnection() {

        socket.onopen = this.openSocket;
        socket.onmessage = this.handleDriverRealTime;
        socket.onerror = this.closeSocket;
        socket.onclose = this.errorSocket;
    }

    openSocket = (event) => {
        if ( event !== null ) {
            this.setState({ socketconnection: true });
        }
    }
    closeSocket = (event) => {
        this.setState({ socketconnection: false });
    }
    errorSocket = (event) => {
        this.setState({ socketconnection: false });
    }

    /**
     * 
     */
    handleDriverRealTime = (event) => {
        try {
            let data = JSON.parse( event.data );
            switch ( data.type ) {
                case 'take': this.removeFromList( data.data.trip ); break;
                case "newcall": this.renderNewTrip(data);
                default: break;
            }
        }
        catch {
        }
    }

    /**
     * updates state for list of trips to draw new trip row 
     */
    renderNewTrip = (tripData) => {
        if ( this.state.onTrip == false ) {
            if ( this.state.location != null ) {
                let cliCord = tripData.data.pickupmapscoord.split(",");
                let myLat = this.state.location.coords.latitude;
                let myLng = this.state.location.coords.longitude;
                let cliLat = cliCord[0];
                let cliLng = cliCord[1];
                let myDistance = getKilometros( myLat, myLng, cliLat, cliLng );

                // distance
                let distanceAllowed = 0.5;
                switch ( tripData.data.nivel ) {
                    case 1: distanceAllowed = 0.5; break;
                    case 2: distanceAllowed = 1; break;
                    case 3: distanceAllowed = 1.5; break;
                    case 4: distanceAllowed = 20; break;
                }

                if ( myDistance < distanceAllowed ) {
                    tripData.data.myDistance = myDistance;
                    let trips = this.state.trips;
                    if ( trips.indexOf(tripData.data) == -1 ) {
                        trips.push(tripData.data);
                        this.setState({trips});
                    }
                    
                }
            }
        }
    }

    /**
     * takes a trip from the list and open modal to show its information
     * @param {object} trip trip data
     */
    selectTrip = async (trip) => {
        let cliCord = trip.pickupmapscoord.split(",");

        let myLat = this.state.location.coords.latitude;
        let myLng = this.state.location.coords.longitude;
        let cliLat = cliCord[0];
        let cliLng = cliCord[1];

        let myDistance = getKilometros( myLat, myLng, cliLat, cliLng );
        
        if ( myDistance < 2 ) { // si la distancia es menor a dos kilometros, se puede tomar el viaje
            if ( await this.handleTrip('take', trip.id) ) { // trip.id tiene el ID de la llamada (calls); trip.trip tiene el id del trip
                this.setState({ trips: [], onTrip: true, currentTrip: trip });
                
                socket.send(JSON.stringify({ 
                        type: "take", 
                        sender: this.state.driverId,
                        data: {
                            trip: trip.trip,
                            id: trip.id
                        }}));
            }
            else {
                this.removeFromList( trip.trip );
            }
        }
        else {
            Alert.alert( APPNAME, "You are too far." );
        }
    }

    /**
     * se comunica con la API para verificar el comportamiento de un viaje
     * @param {string} action la accion a solicitar en la API
     * @param {int} tripId el ID del viaje en cuestion
     * @return {true} si la accion se ejecuta correctamente
     * @return {false} en caso de que no se pueda ejecutar
     */
    handleTrip = async (action, tripId) => {
        let url;
        switch ( action ) {
            case 'take': url = UrlTakeTrip; break;
            case 'start': url = UrlStartTrip; break;
            case 'end': url = UrlEndTrip; break;
            default: url = UrlTakeTrip; break;
        }
        let datos = {driver: this.state.driverId, trip: tripId}
        let response = await fetch(url, {
                method: 'POST', // or 'PUT'
                body: JSON.stringify(datos), // data can be `string` or {object}!
                headers:{
                    'Content-Type': 'application/json'
                }
        }).then(res => res.json()).catch(error => { return '{"status":"error"}' }).then(response => { return response; });
        if ( typeof(response) === 'object' ) {
            if ( response.status === 'ok' ) {
                return true;
            }
            else {
                Alert.alert( APPNAME, response.message,[ { text: "OK",  } ], { cancelable: false } );
            }
        }
        else {
            Alert.alert( APPNAME, "Network error.",[ { text: "OK",  } ], { cancelable: false } );
        }
        return false;
    }
    
    /**
     * quita un viaje de la lista de viajes
     * @param {int} tripId el ID del trip que se debe remover de la lista
     */
    removeFromList = (tripId) => {
        console.log("REMOVER =========", tripId);
        let trips = this.state.trips;
        let tmpTrips = new Array();
        let j=0;
        for ( let i=0; i<trips.length; i++ ) {
            if ( trips[i].trip != tripId ) {
                tmpTrips[j] = trips[i];
                j++;
            }
        }
        this.setState({ trips: tmpTrips });
    }

    /**
     * gets current location of this device every 20 seconds and sends to central server
     */
    getLocation = async () => {
        const { status } = await Permissions.askAsync( Permissions.LOCATION );
        if ( status !== "granted" ) {
            return 0;
        }
        let location = await Location.getCurrentPositionAsync();
        let time = new Date();
        
        location.coords.driverId = this.state.driverId;
        location.coords.time = time.getHours() + ":" + time.getMinutes() + ":"  + time.getSeconds();

        socket.send(JSON.stringify({ type:"location", data:location.coords, sender: this.state.driverId }));
        this.setState({ location });
    }

    /**
     * Opens GPS application to navigate to client's location
     */
    goToClientLocation = () => {
        
        let cordenadas = this.state.currentTrip.pickupmapscoord.split(',');
        
        let lat = cordenadas[0];
        let lng = cordenadas[1];

        Alert.alert(APPNAME, "Do you want to open navigation app?", 
        [
            { text: "No", style: "cancel" },
            { text: "Yes", onPress: () => this.navigateTo( lat, lng ) }
        ],
        { cancelable: false });
    }
    
    /**
     * validates the location of driver and if it is close to client's location
     * fires GPS app in their phone to start navigation to destination client
     */
    startTrip = async () => {
        if ( this.state.location != null ) {
            let cliCord = this.state.currentTrip.pickupmapscoord.split(",");

            let myLat = this.state.location.coords.latitude;
            let myLng = this.state.location.coords.longitude;
            let cliLat = cliCord[0];
            let cliLng = cliCord[1];
            
            let myDistance = getKilometros( myLat, myLng, cliLat, cliLng );

            if ( myDistance <= 0.050 ) { // a menos de 50 metros se puede considerar que esta en la posicion del cliente
                let destCord = this.state.currentTrip.pickupmapscoord.split(",");
                let destLat = destCord[0];
                let destLng = destCord[1];
                if ( await this.handleTrip('start', this.state.currentTrip.id) ) {
                    Alert.alert(APPNAME, "Do you want to open navigation app?", 
                    [ { text: "No", style: "cancel" }, { text: "Yes", onPress: () => this.navigateTo( destLat, destLng ) } ],
                    { cancelable: false });
                    this.setState({ongoing: true});
                    socket.send(JSON.stringify(  { 
                        type: "starttrip", 
                        driver: this.state.driverId,
                        client: this.state.currentTrip.client,
                        data: {
                            trip: this.state.currentTrip.trip,
                            id: this.state.currentTrip.id
                        }
                    } ) );
                }
            }
            else {
                Alert.alert(APPNAME, "You are not at client's location,");
            }
        }
        else {
            Alert.alert(APPNAME, "Please wait for location service.");
        }
    }

    /**
     * abre la aplicacion predeterminada de navegacion
     * @param {string} lat coordenadas de latitud 
     * @param {string} lng coordenadas de longitud
     */
    navigateTo = ( lat, lng) => {
        if ( Platform.OS === 'ios' ) {            
            Linking.openURL('maps://app?daddr='+lat+'+'+lng);
        }
        else {
            Linking.openURL('google.navigation:q='+lat+'+'+lng); 
        } 
    }

    /**
     * send signal for finnish a trip and allows driver to receive new trips
     * @param {string} destinationCoords las coordenadas de destino
     */
    endTrip = async (destinationCoords) => {
        if ( this.state.location != null ) {
            let cliCord = destinationCoords.split(",");

            let myLat = this.state.location.coords.latitude;
            let myLng = this.state.location.coords.longitude;
            let cliLat = cliCord[0];
            let cliLng = cliCord[1];
            
            let myDistance = getKilometros( myLat, myLng, cliLat, cliLng );

            if ( myDistance <= 0.050 ) {
                if ( await this.handleTrip('end', this.state.currentTrip.trip) ) {
                    socket.send(JSON.stringify( { 
                        client: this.state.currentTrip.client, 
                        action: "endtrip",
                        data: {
                            trip: this.state.currentTrip.trip,
                            driver: this.state.driverId
                        }
                    } ) );
                    this.setState({onTrip: false, currentTrip: {}, ongoing: false, notified: false});
                    return true;
                }
            }
            else {
                Alert.alert(APPNAME, "You are not at client's destination.");
            }
        }
        else {
            Alert.alert(APPNAME, "Please wait for location service.");
        }
        return false;
    }

    /**
     * envia una alerta el cliente avisando que su coche ya esta en su ubicacion
     */
    notifyClient = () => {
        if ( this.state.location != null ) {
            let cliCord = this.state.currentTrip.pickupmapscoord.split(",");

            let myLat = this.state.location.coords.latitude;
            let myLng = this.state.location.coords.longitude;
            let cliLat = cliCord[0];
            let cliLng = cliCord[1];
            
            let myDistance = getKilometros( myLat, myLng, cliLat, cliLng );

            if ( myDistance <= 0.050 && this.state.notified === false ) { // a menos de 50 metros, se puede considerar que esta en la posicion del cliente
                socket.send(JSON.stringify(  { 
                    client: this.state.currentTrip.client, 
                    action: "notify",
                    data: "Your drivier has arrived."
                } ));
                this.setState({ notified: true });
            }
            else {
                Alert.alert(APPNAME, "You are not at client's location.");
            }
        }
    }

    render() {
        let statusLocation = "Off"; let statusSocket = "Off"; let statusApp = "Off";
        if ( this.state.location != null ) { statusLocation = "On"; }
        if (this.state.socketconnection != false) { statusSocket = "On"; }
        if (this.state.driverId != false) { statusApp = "On"; }
        return(
            <View style={GlobalStyles.container}>
                <View style={TripsStyles.container}>
                    <View style={GlobalStyles.header}>
                        <Text style={GlobalStyles.textHeader}>Trips</Text>
                    </View>
                    
                    <FlatList
                        data={this.state.trips}
                        renderItem={ ({item}) => <TripItem street={item.pickupstreet} eta={item.distance} myDistance={item.myDistance} selectTrip={ () => this.selectTrip(item) } /> }
                        keyExtractor={ item => item.id.toString() } />
                    
                    <View style={TripsStyles.tripListStatus}>
                        <Text style={GlobalStyles.textHeader}>Services</Text>
                        <Text style={GlobalStyles.textNormal}>Location: {statusLocation}</Text>
                        <Text style={GlobalStyles.textNormal}>Communication: {statusSocket}</Text>
                        <Text style={GlobalStyles.textNormal}>Application: {statusApp}</Text>
                    </View>
                    
                    <TripModal 
                        showTrip={this.state.onTrip} 
                        ongoing={this.state.ongoing}
                        trip={this.state.currentTrip} 
                        toClientLocation={this.goToClientLocation}
                        startTrip={this.startTrip}
                        endTrip={this.endTrip}
                        notifyClient={this.notifyClient}
                    />

                </View>
                
            </View>
        );
    }
}