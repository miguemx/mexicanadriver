import React from "react";
import { View, ActivityIndicator, StatusBar, Text, Button } from "react-native";


import { GlobalStyles } from "../styles/"

export default class CargaScreen extends React.Component {
    render() {
        return(
            <View style={GlobalStyles.container}>
                <ActivityIndicator />
                <StatusBar barStyle="default" />
                <Text>{this.props.extraData}</Text>
                <Button title="ir a login" onPress={() => this.props.navigation.navigate("Login",{tripId: 125})} />
            </View>
        );
    }
}